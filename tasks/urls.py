from django.urls import path
from tasks.views import create_task, show_tasks, edit_task, delete_task, search_tasks

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_tasks, name="show_my_tasks"),
    path("edit/<int:id>/", edit_task, name="edit_task"),
    path("delete/<int:id>/", delete_task, name="delete_task"),
    path("search_results/", search_tasks, name="search_tasks"),
]
