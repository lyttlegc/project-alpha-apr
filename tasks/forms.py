from django.forms import ModelForm
from tasks.models import Task
from django import forms


class DateInput(forms.DateInput):
    input_type = "date"


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
            "is_completed",
        ]
        widgets = {
            "start_date": DateInput(),
            "due_date": DateInput(),
        }
