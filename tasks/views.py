from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(request.session["return_to"])
    else:
        request.session["return_to"] = request.META.get("HTTP_REFERER")
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/show_tasks.html", context)


@login_required
def edit_task(request, id):
    task_instance = Task.objects.get(id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task_instance)
        if form.is_valid():
            task_instance = form.save()
            return redirect(request.session["return_to"])
    else:
        request.session["return_to"] = request.META.get("HTTP_REFERER")
        form = TaskForm(instance=task_instance)
    context = {
        "form": form,
        "task": task_instance,
    }
    return render(request, "tasks/edit_task.html", context)


@login_required
def delete_task(request, id):
    task_instance = Task.objects.get(id=id)
    if request.method == "POST":
        task_instance.delete()
        return redirect(request.session["return_to"])
    else:
        request.session["return_to"] = request.META.get("HTTP_REFERER")
    context = {
        "task": task_instance,
    }
    return render(request, "tasks/delete_task.html", context)


@login_required
def search_tasks(request):
    if request.method == "POST":
        searched = request.POST["searched"]
        tasks = Task.objects.filter(name__contains=searched)
        context = {
            "searched": searched,
            "tasks": tasks,
        }
        return render(request, "tasks/search_tasks.html", context)
    else:
        return render(request, "tasks/search_tasks.html", context)
