from django.forms import ModelForm
from projects.models import Project, Comment


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            "name",
            "body",
            "project",
        ]
