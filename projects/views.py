from django.shortcuts import render, redirect
from projects.models import Project, Comment
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm, CommentForm


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)


@login_required
def edit_project(request, id):
    project_instance = Project.objects.get(id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project_instance)
        if form.is_valid():
            project_instance = form.save()
            return redirect("show_project", id=project_instance.id)
    else:
        form = ProjectForm(instance=project_instance)
    context = {
        "form": form,
        "project": project_instance,
    }
    return render(request, "projects/edit_project.html", context)


@login_required
def delete_project(request, id):
    project_instance = Project.objects.get(id=id)
    if request.method == "POST":
        project_instance.delete()
        return redirect("list_projects")
    context = {
        "project": project_instance,
    }
    return render(request, "projects/delete_project.html", context)


@login_required
def create_comment(request):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(request.session["return_to"])
    else:
        request.session["return_to"] = request.META.get("HTTP_REFERER")
        form = CommentForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_comment.html", context)


@login_required
def delete_comment(request, id):
    comment_instance = Comment.objects.get(id=id)
    if request.method == "POST":
        comment_instance.delete()
        return redirect(request.session["return_to"])
    else:
        request.session["return_to"] = request.META.get("HTTP_REFERER")
    context = {
        "comment": comment_instance,
    }
    return render(request, "projects/delete_comment.html", context)
