from django.db import models
from django.conf import settings


# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name


class Comment(models.Model):
    name = models.CharField(max_length=100, default="DEFAULT VALUE")
    date_posted = models.DateTimeField(auto_now_add=True)
    body = models.TextField()
    project = models.ForeignKey(
        Project,
        related_name="comments",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-date_posted"]
