from django.urls import path
from projects.views import (
    list_projects,
    show_project,
    create_project,
    delete_project,
    edit_project,
    create_comment,
    delete_comment,
)

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("delete/<int:id>/", delete_project, name="delete_project"),
    path("edit/<int:id>/", edit_project, name="edit_project"),
    path("create-comment/", create_comment, name="create_comment"),
    path("delete_comment/<int:id>/", delete_comment, name="delete_comment"),
]
